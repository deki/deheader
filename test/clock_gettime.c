/*
 * Items: clock_gettime(
 * Standardized-By: SuS
 * Detected-by: gcc-4.4.3 + Linux
 */

#include <time.h>

main(int arg, char **argv)
{
    struct timespec ts;
    (void)clock_gettime(0, &ts);
}
