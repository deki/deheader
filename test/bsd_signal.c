/*
 * Items: bsd_signal(
 * Standardized-By: SuS
 * Not-Detected-by: gcc-4.4.3 + Linux
 */

#include <signal.h>

void handler(int sig)
{
}

main(int arg, char **argv)
{
    (void) bsd_signal(0, handler);
}
